<?php

// Add styles and scripts
function oas_scripts() {
  wp_enqueue_style ( 'oas_styles', get_template_directory_uri() . '/watch/scripts.css' );
  wp_enqueue_script ( 'oas_scripts', get_template_directory_uri() . '/watch/scripts.js' );
}
add_action('wp_enqueue_scripts', 'oas_scripts');