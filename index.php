<body>
    <?php get_header(); ?>

    <div class="container">

        <div class="container-inner">

            <h1 class="heading h1">[h1] A wonderful serenity.</h1>
            <p class="paragraph p">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas perferendis soluta, non possimus, temporibus iste
                impedit cum atque deserunt dolorem officiis illum, debitis velit repellendus tempora asperiores? Repellendus,
                odio commodi? Facilis autem doloremque id itaque, voluptas minima eaque quam necessitatibus eligendi non laborum.
                Porro nesciunt ducimus fuga quia fugit consequatur a praesentium voluptate, esse atque, deserunt sequi.
                Ipsum, aliquid nisi. Similique maxime culpa veritatis inventore nesciunt id totam magni, quibusdam officia.
            </p>

            <h2 class="heading h2">[h2] A wonderful serenity.</h2>
            <p class="paragraph p">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas perferendis soluta, non possimus, temporibus iste
                impedit cum atque deserunt dolorem officiis illum, debitis velit repellendus tempora asperiores? Repellendus,
                odio commodi? Facilis autem doloremque id itaque, voluptas minima eaque quam necessitatibus eligendi non laborum.
                Porro nesciunt ducimus fuga quia fugit consequatur a praesentium voluptate, esse atque, deserunt sequi.
                Ipsum, aliquid nisi. Similique maxime culpa veritatis inventore nesciunt id totam magni, quibusdam officia.
            </p>

            <h3 class="heading h3">[h3] A wonderful serenity.</h3>
            <p class="paragraph p">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas perferendis soluta, non possimus, temporibus iste
                impedit cum atque deserunt dolorem officiis illum, debitis velit repellendus tempora asperiores? Repellendus,
                odio commodi? Facilis autem doloremque id itaque, voluptas minima eaque quam necessitatibus eligendi non laborum.
                Porro nesciunt ducimus fuga quia fugit consequatur a praesentium voluptate, esse atque, deserunt sequi.
                Ipsum, aliquid nisi. Similique maxime culpa veritatis inventore nesciunt id totam magni, quibusdam officia.
            </p>

            <h4 class="heading h4">[h4] A wonderful serenity.</h4>
            <p class="paragraph p">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas perferendis soluta, non possimus, temporibus iste
                impedit cum atque deserunt dolorem officiis illum, debitis velit repellendus tempora asperiores? Repellendus,
                odio commodi? Facilis autem doloremque id itaque, voluptas minima eaque quam necessitatibus eligendi non laborum.
                Porro nesciunt ducimus fuga quia fugit consequatur a praesentium voluptate, esse atque, deserunt sequi.
                Ipsum, aliquid nisi. Similique maxime culpa veritatis inventore nesciunt id totam magni, quibusdam officia.
            </p>


            <h5 class="heading h5">[h5] A wonderful serenity.</h5>
            <p class="paragraph p">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas perferendis soluta, non possimus, temporibus iste
                impedit cum atque deserunt dolorem officiis illum, debitis velit repellendus tempora asperiores? Repellendus,
                odio commodi? Facilis autem doloremque id itaque, voluptas minima eaque quam necessitatibus eligendi non laborum.
                Porro nesciunt ducimus fuga quia fugit consequatur a praesentium voluptate, esse atque, deserunt sequi.
                Ipsum, aliquid nisi. Similique maxime culpa veritatis inventore nesciunt id totam magni, quibusdam officia.
            </p>

            <h6 class="heading h6">[h6] A wonderful serenity.</h6>
            <p class="paragraph p">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas perferendis soluta, non possimus, temporibus iste
                impedit cum atque deserunt dolorem officiis illum, debitis velit repellendus tempora asperiores? Repellendus,
                odio commodi? Facilis autem doloremque id itaque, voluptas minima eaque quam necessitatibus eligendi non laborum.
                Porro nesciunt ducimus fuga quia fugit consequatur a praesentium voluptate, esse atque, deserunt sequi.
                Ipsum, aliquid nisi. Similique maxime culpa veritatis inventore nesciunt id totam magni, quibusdam officia.
            </p>

        </div>

    </div>

    <?php get_footer(); ?>
</body>
</html>